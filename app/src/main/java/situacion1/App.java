
package situacion1;


public class App {

    public static void main(String[] args) {
        //Se define un empleado
        Comisionado nico = new Comisionado(
        "Nicolas", 
        "Ferreyra", 
        300000f);
        Pago pago1 = new Pago();

        //Se realiza un pago a un empleado comisionado que supera el monto de asiganacion libre, por lo que se le retiene el 20%
        pago1.pagarComision(nico);

        //Se le da una bonificacion semanal por ventas exitosas (en caso de no tenerlas el monto sera 0)
        pago1.darBonificacionSemanal(nico);

        //Se le paga el sueldo
        pago1.PagarSueldos(nico);

        //Se define un empleado asalariado 
        Asalariado fran = new Asalariado(
        "Federico",
        "Diaz", 
        70000f);

        //Como el monto de su sueldo no supera la asignacion libre de impuestos no se le retiene nada
        pago1.PagarSueldos(fran);

        //Se hacen pagos a una empresa contratante
        pago1.pagarEmpresaContratante();
    }
}
