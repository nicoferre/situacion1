package situacion1;



public abstract class Empleado extends Persona {
    private Float sueldoBase;
    
    


    public Empleado(String nombre, String apellido, Float sueldoBase){
        super(nombre, apellido);
        this.sueldoBase=sueldoBase;
    }

    public String getNombre(){
        return nombre;
    }

    public String getApellido(){
        return apellido;
    }

    public Float getSueldoBase(){
        return sueldoBase;
    }

    public void setSueldo(Float sueldoBase){
        this.sueldoBase=sueldoBase;
    }

    

    public void Comision(Float comision){
        this.sueldoBase += comision;
    }

    
}
