package situacion1;

import java.util.Scanner;

import org.checkerframework.checker.units.qual.s;

import java.util.List;
import java.util.ArrayList;

public class Pago {
    private Float asignacionLibreImpuestos = 280792f;
    List<Float> pagosEmpresaContratante = new ArrayList();
    Scanner entrada = new Scanner(System.in);
    

    public Pago(){

    }

    public void darBonificacionSemanal(Comisionado empleado){
        empleado.recibirComisiones(empleado.getSueldoBase()*0.5f);

    }

    public void pagarComision(Comisionado empleado){
        System.out.println("MONTO DE LA COMISION A PAGAR: ");
        String MontoaPagar = entrada.nextLine();
        empleado.recibirComisiones(Float.parseFloat(MontoaPagar));
    }

    

    public void pagarEmpresaContratante(){
        String pago, opcion;
        Float pagoTotal=0f;
        
        do{
            System.out.println("MONTO A PAGAR A EMPRESAS: ");
            pago = entrada.nextLine();
            pagosEmpresaContratante.add(Float.parseFloat(pago));
            System.out.println("INGRESE 1 PARA SEGUIR REALIZANDO PAGOS/ 0 PARA DEJAR DE HACERLO");
            opcion= entrada.nextLine();
        }while(Integer.parseInt(opcion)==1);
        for(int i=0;i<pagosEmpresaContratante.size();i++){
            pagoTotal += pagosEmpresaContratante.get(i);
            System.out.println("::::::FACTURA::::::");
            System.out.println(" ");
            System.out.println("Servicio nro: "+(i+1)+" ....... "+pagosEmpresaContratante.get(i));
            System.out.println(" ");
            System.out.println(":::::::::::::::::::");
        }
        System.out.println("\n\n");
        System.out.println("SE HA PAGADO EN TOTAL A LA EMPRESA: " +pagoTotal);
        System.out.println(":::::::::::::::::::::::::::::::::");
    }

    public void PagarSueldos(Empleado empleado){
        if(empleado.getSueldoBase()>asignacionLibreImpuestos){
            retenerImpuestos(empleado);
        }
        System.out.println("El salario de "+ empleado.getApellido() +" "+ empleado.getNombre()+" es: " + empleado.getSueldoBase());
    }


    public void retenerImpuestos(Empleado empleado){
        empleado.setSueldo(empleado.getSueldoBase()-(empleado.getSueldoBase()*0.2f)); 
    }

}
